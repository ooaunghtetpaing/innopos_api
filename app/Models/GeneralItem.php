<?php

namespace App\Models;

use App\Traits\BasicAudit;
use App\Traits\HistoryRecord;
use App\Traits\SnowflakeID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralItem extends Model
{
    use HasFactory, SnowflakeID, BasicAudit, SoftDeletes, HistoryRecord;

    protected $fillable = [
        'item_category_id',
        'item_cover_photo',
        'item_photos',
        'name',
        'model',
        'code',
        'label',
        'description',
        'other_name',
        'make_in',
        'packing',
        'prebox',
        'is_inventory',
        'status',
    ];

    protected $casts = [
        'item_photos' => 'array',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(ItemCategory::class, 'item_category_id');
    }
}
