<?php

namespace App\Models;

use App\Traits\BasicAudit;
use App\Traits\HistoryRecord;
use App\Traits\SnowflakeID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerAddress extends Model
{
    use HasFactory, SnowflakeID, BasicAudit, SoftDeletes, HistoryRecord;

    protected $fillable = [
        'customer_id', 'address', 'is_default', 'status',
    ];

    protected $casts = [
        'is_default' => 'boolean',
    ];
}
