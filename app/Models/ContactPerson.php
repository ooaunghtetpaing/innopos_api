<?php

namespace App\Models;

use App\Traits\BasicAudit;
use App\Traits\HistoryRecord;
use App\Traits\SnowflakeID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactPerson extends Model
{
    use HasFactory, SoftDeletes, SnowflakeID, BasicAudit, HistoryRecord;

    protected $fillable = [
        'business_id', 'name', 'phone', 'email', 'address', 'position', 'status',
    ];
}
