<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = [
        'name', 'description', 'permissions',
    ];

    protected $hidden = [
        'guard_name',
    ];

    protected $casts = [
        'permissions' => 'array',
    ];
}
