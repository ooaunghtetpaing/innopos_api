<?php

namespace App\Models;

use App\Traits\BasicAudit;
use App\Traits\HistoryRecord;
use App\Traits\SnowflakeID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model
{
    use HasFactory, SoftDeletes, SnowflakeID, BasicAudit, HistoryRecord;

    protected $fillable = [
        'name', 'phone', 'email', 'address', 'type', 'status',
    ];

    public function contactPerson()
    {
        return $this->hasMany(ContactPerson::class);
    }
}
