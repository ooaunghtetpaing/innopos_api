<?php

namespace App\Models;

use App\Traits\BasicAudit;
use App\Traits\HistoryRecord;
use App\Traits\SnowflakeID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use HasFactory, SnowflakeID, BasicAudit, SoftDeletes, HistoryRecord;

    protected $table = 'purchase';

    protected $fillable = [
        'business_name', 'business_type', 'business_email', 'business_phone', 'business_address',
        'contact_person', 'contact_person_phone', 'contact_person_email',
        'discount', 'tax', 'other', 'total_amount', 'pay_amount',
        'payment_method', 'bank_type', 'bank_account', 'bank_holdername',
        'credit', 'disposit',
        'status',
    ];

    public function items()
    {
        return $this->hasMany(PurchaseItem::class, 'purchase_id');
    }
}
