<?php

namespace App\Enums;

enum BankTypeEnum: string
{
    case AYAR = 'AYAR';
    case KBZ = 'KBZ';
    case UAB = 'UAB';
    case CB = 'CB';
    case AGD = 'AGD';
    case KPAY = 'KPAY';
    case CBPAY = 'CBPAY';
    case UABPAY = 'UABPAY';
    case ONEPAY = 'ONEPAY';
    case WAVE_MONEY = 'WAVE_MONEY';
    case VISA_MASTER = 'VISA_MASTER';
    case AYAR_PAY = 'AYAR_PAY';
}
