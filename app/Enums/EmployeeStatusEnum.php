<?php

namespace App\Enums;

enum EmployeeStatusEnum: string
{
    case SUSPANDED = 'SUSPANDED';
    case CURRENT = 'CURRENT';
    case LEAVE = 'LEAVE';
    case PENDING = 'PENDING';
}
