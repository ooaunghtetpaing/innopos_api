<?php

namespace App\Enums;

enum PurchaseStatusEnum: string
{
    case PENDING = 'PENDING';
    case APPROVED = 'APPROVED';
}
