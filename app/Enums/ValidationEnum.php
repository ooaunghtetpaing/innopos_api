<?php

namespace App\Enums;

enum ValidationEnum: string
{
    case MIMES = 'jpg,png,jpeg,gif,svg';
}
