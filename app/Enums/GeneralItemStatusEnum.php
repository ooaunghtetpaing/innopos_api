<?php

namespace App\Enums;

enum GeneralItemStatusEnum: string
{
    case ACTIVE = 'ACTIVE';
    case DISABLE = 'DISABLE';
}
