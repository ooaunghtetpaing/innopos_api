<?php

namespace App\Enums;

enum UserStatusEnum: string
{
    case ACTIVE = 'ACTIVE';
    case SUSPEND = 'SUSPEND';
    case LOCKED = 'LOCKED';
}
