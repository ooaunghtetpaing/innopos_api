<?php

namespace App\Enums;

enum BusinessTypeEnum: string
{
    case COMPANY = 'COMPANY';
    case WHOLESALE = 'WHOLESALE';
}
