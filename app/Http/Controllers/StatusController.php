<?php

namespace App\Http\Controllers;

use App\Enums\BankTypeEnum;
use App\Enums\BusinessTypeEnum;
use App\Enums\EmployeeStatusEnum;
use App\Enums\GeneralItemStatusEnum;
use App\Enums\GeneralStatusEnum;
use App\Enums\PaymentMethodEnum;
use App\Enums\PurchaseStatusEnum;
use App\Enums\UserStatusEnum;
use App\Helpers\Enum;
use Illuminate\Http\Request;

/**
 * @group General Management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class StatusController extends Controller
{
    /**
     * APIs for retrive status record [support multiple status type]
     *
     * @urlParam type string. Example: user,business_type,employee,item,general,purchase
     */
    public function index(Request $request)
    {
        $requestStatus = explode(',', $request->get('type'));

        $allowableStatus = [
            'user' => (new Enum(UserStatusEnum::class))->values(),
            'purchase' => (new Enum(PurchaseStatusEnum::class))->values(),
            'general' => (new Enum(GeneralStatusEnum::class))->values(),
            'item' => (new Enum(GeneralItemStatusEnum::class))->values(),
            'employee' => (new Enum(EmployeeStatusEnum::class))->values(),
            'business_type' => (new Enum(BusinessTypeEnum::class))->values(),
            'bank_type' => (new Enum(BankTypeEnum::class))->values(),
            'payment_method' => (new Enum(PaymentMethodEnum::class))->values(),
        ];

        $statusTypes = collect($allowableStatus)->filter(function ($value, $index) use ($requestStatus) {
            return in_array($index, $requestStatus);
        });

        return $this->success('status type list are successfully retrived', $statusTypes);
    }
}
