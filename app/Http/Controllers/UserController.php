<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group User management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class UserController extends Controller
{
    /**
     * APIs for retrive users record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     * @urlParam status string. Example: ACTIVE, SUSPEND, LOCKED
     */
    public function index(Request $request)
    {

        $type = $request->query('type') ?? null;
        $filter = $request->query('filter') ?? null;
        $value = $request->query('value') ?? null;

        DB::beginTransaction();

        try {
            $users = User::with(['employee'])
                ->when($tyacpe === 'employee', function ($builder) {
                    return $this->getEmployeeUser($builder);
                })
                ->when($type === 'employee' && $filter && $value, function ($builder) use ($filter, $value) {
                    return $this->getEmployeeFilter($builder, $filter, $value);
                })
                ->when($type === 'user' && $filter && $value, function ($builder) use ($filter, $value) {
                    return $this->getUserFilter($builder, $filter, $value);
                })
                ->when(! $type, function ($builder) {
                    return $builder->doesnthave('employee');
                })
                ->searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('users is successfully retrived', $users);
        } catch (Exception $e) {
            throw $e;
        }

    }

    /**
     * APIs for create new user record
     *
     * @bodyParam name required.
     * @bodyParam username required.
     * @bodyParam profile.
     * @bodyParam email required.
     * @bodyParam phone required.
     * @bodyParam password required.
     * @bodyParam password_confirmation required.
     */
    public function store(UserStoreRequest $request)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $payload['password'] = bcrypt($payload['password']);
            $user = User::create($payload->toArray());
            DB::commit();

            return $this->success('new user is successfully created', $user);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * APIs for show user record by id
     */
    public function show($id)
    {
        DB::beginTransaction();

        try {
            $user = User::with(['employee'])
                ->FindOrFail($id);
            DB::commit();

            return $this->success('user is successfully retrived', $user);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * APIs for update user record
     *
     * @bodyParam name.
     * @bodyParam username.
     * @bodyParam profile.
     * @bodyParam email.
     * @bodyParam phone.
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $user = User::FindOrFail($id);
            $user->update($payload->toArray());
            DB::commit();

            return $this->success('user is successfully updated', $user);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * APIs for delete user record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $user = User::FindOrFail($id);
            $user->delete($id);
            DB::commit();

            return $this->success('user is successfully deleted', null);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     *  Get employee user list
     *
     *  @urlPrams type
     */
    private function getEmployeeUser($builder)
    {
        return $builder->whereHas('employee');
    }

    /**
     *  Get employee user list with fitler
     *
     *  @urlParams [type, filter, value]
     */
    private function getEmployeeFilter($builder, $filter, $value)
    {
        return $builder->whereHas('employee', function ($query) use ($filter, $value) {
            return $query->where([$filter => $value]);
        });
    }

    /**
     *  Get user list with fitler
     *
     *  @urlParams [type, filter, value]
     */
    private function getUserFilter($builder, $filter, $value)
    {
        return $builder->where([$filter => $value]);
    }
}
