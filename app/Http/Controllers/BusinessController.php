<?php

namespace App\Http\Controllers;

use App\Http\Requests\BusinessStoreRequest;
use App\Http\Requests\BusinessUpdateRequest;
use App\Models\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Supplier management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class BusinessController extends Controller
{
    /**
     * APIs for retrive customers record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {

        DB::beginTransaction();
        try {
            $business = Business::with(['contactPerson'])->searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('business list are successfully retrived', $business);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create new business record
     *
     * @bodyParam name required.
     * @bodyParam phone required.
     * @bodyParam email.
     * @bodyParam address.
     * @bodyParam type. Example : COMPANY, WHOLESALE
     */
    public function store(BusinessStoreRequest $request)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $business = Business::create($payload->toArray());
            DB::commit();

            return $this->success('New business is created successfully', $business);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show business record by id
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $business = Business::with(['contactPerson'])->findOrFail($id);
            DB::commit();

            return $this->success('business info is retrieved successfully', $business);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update business record
     *
     * @bodyParam name.
     * @bodyParam phone.
     * @bodyParam email.
     * @bodyParam address.
     * @bodyParam type. Example : COMPANY, WHOLESALE
     */
    public function update(BusinessUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $business = Business::findOrFail($id);
            $business->update($payload->toArray());
            DB::commit();

            return $this->success('bussiness is updated successfully', $business);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete business record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $business = Business::findOrFail($id);
            $business->delete($id);
            DB::commit();

            return $this->success('business is deleted', $business);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
