<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactPersonStoreRequest;
use App\Http\Requests\ContactPersonUpdateRequest;
use App\Models\ContactPerson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Supplier management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class ContactPersonController extends Controller
{
    /**
     * APIs for retrive contact person record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {

        DB::beginTransaction();
        try {
            $contactPerson = ContactPerson::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('contact person list are successfully retrived', $contactPerson);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create new contact person record
     *
     * @bodyParam business_id required.
     * @bodyParam name required.
     * @bodyParam phone required.
     * @bodyParam email.
     * @bodyParam address.
     * @bodyParam position.
     */
    public function store(ContactPersonStoreRequest $request)
    {

        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $contactPerson = ContactPerson::create($payload->toArray());
            DB::commit();

            return $this->success('New contact person is created successfully', $contactPerson);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show business record by id
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $contactPerson = ContactPerson::findOrFail($id);
            DB::commit();

            return $this->success('contact person info is retrieved successfully', $contactPerson);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update contact person record
     *
     * @bodyParam business_id.
     * @bodyParam name.
     * @bodyParam phone.
     * @bodyParam email.
     * @bodyParam address.
     * @bodyParam position.
     */
    public function update(ContactPersonUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $contactPerson = ContactPerson::findOrFail($id);
            $contactPerson->update($payload->toArray());
            DB::commit();

            return $this->success('contact person is updated successfully', $contactPerson);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete contact person record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $contactPerson = ContactPerson::findOrFail($id);
            $contactPerson->delete($id);
            DB::commit();

            return $this->success('contact person is deleted', $contactPerson);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
