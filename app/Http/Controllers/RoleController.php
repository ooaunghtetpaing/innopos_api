<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleStoreRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * APIs for retrive role record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $roles = Role::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('role list are successfully retrived', $roles);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create role record
     *
     * @bodyParam name.
     */
    public function store(RoleStoreRequest $request)
    {
        $payload = collect($request->validated());
        $payload['guard_name'] = 'api';

        DB::beginTransaction();
        try {
            $role = Role::create($payload->toArray());
            DB::commit();

            return $this->success('role is successfully created', $role);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update role record
     *
     * @bodyParam name.
     */
    public function update(RoleUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());

        $roleUpdatePayload = [
            'name' => $payload['name'],
            'description' => $payload['description'],
        ];

        DB::beginTransaction();

        try {
            $role = Role::with(['permissions'])->findOrFail($id);

            $getPermission = $role->toArray()['permissions'];

            $permissions = collect($getPermission)->map(function ($permission) {
                return $permission['id'];
            })->toArray();

            $role->update($roleUpdatePayload);

            $role->revokePermissionTo($permissions);
            $role->syncPermissions($payload['permissions']);

            DB::commit();

            return $this->success('role is successfully updated', $role);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show role record by id
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $role = Role::with('permissions')->findOrFail($id);

            DB::commit();

            return $this->success('role detail is successfully retrived', $role);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete role record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $role = Role::findOrFail($id);

            User::with('roles')->get()->filter(function ($user) use ($role) {
                $user->roles->where('name', $role->name)->filter(function ($hasRole) use ($user, $role) {
                    $permissions = $hasRole->permissions->pluck('name')->toArray();
                    $hasRole->revokePermissionTo($permissions);
                    $user->removeRole($role->name);
                });
            });

            $role->delete($id);
            DB::commit();

            return $this->success('role detail is successfully retrived', $role);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
