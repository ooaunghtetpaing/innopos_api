<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerStoreRequest;
use App\Http\Requests\CustomerUpdateRequest;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Customer management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class CustomerController extends Controller
{
    /**
     * APIs for retrive customers record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {

        DB::beginTransaction();
        try {
            $customers = Customer::with(['addresses'])->searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('Customer infos are successfully retrived', $customers);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create new customer record
     *
     * @bodyParam profile_picture.
     * @bodyParam full_name required.
     * @bodyParam email.
     * @bodyParam phone required.
     * @bodyParam company_name.
     * @bodyParam position.
     */
    public function store(CustomerStoreRequest $request)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $customer = Customer::create($payload->toArray());
            DB::commit();

            return $this->success('New customer is created successfully', $customer);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show customer record by id
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $customer = Customer::with(['addresses'])->findOrFail($id);
            DB::commit();

            return $this->success('Customer info is retrieved successfully', $customer);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update customer record
     *
     * @bodyParam profile_picture.
     * @bodyParam full_name required.
     * @bodyParam email.
     * @bodyParam phone required.
     * @bodyParam company_name.
     * @bodyParam position.
     * @bodyParam status.
     */
    public function update(CustomerUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $customer = Customer::findOrFail($id);
            $customer->update($payload->toArray());
            DB::commit();

            return $this->success('Customer is updated successfully', $customer);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete user record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $customer = Customer::findOrFail($id);
            $customer->delete($id);
            DB::commit();

            return $this->success('customer is deleted', $customer);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
