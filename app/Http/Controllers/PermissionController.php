<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    /**
     * APIs for retrive permission record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $permissions = Permission::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('permission list are successfully retrived', $permissions);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show permission record by id
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $permission = Permission::findOrFail($id);
            DB::commit();

            return $this->success('permission detail is successfully retrived', $permission);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
