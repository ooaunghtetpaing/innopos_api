<?php

namespace App\Http\Controllers;

use App\Http\Requests\PurchaseStoreRequest;
use App\Http\Requests\PurchaseUpdateRequest;
use App\Models\Business;
use App\Models\ContactPerson;
use App\Models\Purchase;
use App\Models\PurchaseItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Purchase management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class PurchaseController extends Controller
{
    /**
     * APIs for retrive purchase item records
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,label,status,description
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $purchase = Purchase::with(['items'])
                ->searchQuery()
                ->sortingQuery()
                ->paginationQuery();
            DB::commit();

            return $this->success('purhcasing lists are successfully retrived', $purchase);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

    }

    /**
     * APIs for create new purchase item record
     *
     * @bodyParam contact_id required.
     * @bodyParam business_id required.
     * @bodyParam item_id required.
     * @bodyParam discount.
     * @bodyParam tax.
     * @bodyParam other.
     * @bodyParam total_amount required.
     * @bodyParam pay_amount required.
     * @bodyParam payment_method required
     * @bodyParam bank_type
     * @bodyParam bank_account
     * @bodyParam bank_holdername
     * @bodyParam purchase_items required.
     */
    public function store(PurchaseStoreRequest $request)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $business = Business::findOrFail($payload['business_id']);

            $purchasePayload = [];

            if ($business) {
                $purchasePayload = array_merge($purchasePayload, [
                    'business_name' => $business['name'],
                    'business_type' => $business['type'],
                    'business_email' => $business['email'],
                    'business_phone' => $business['phone'],
                    'business_address' => $business['address'],
                ]);
            }

            $contactPerson = ContactPerson::findOrFail($payload['contact_id']);

            if ($contactPerson) {
                $purchasePayload = array_merge($purchasePayload, [
                    'contact_person' => $contactPerson['name'],
                    'contact_person_phone' => $contactPerson['phone'],
                    'contact_person_email' => $contactPerson['email'],
                ]);
            }

            $purchasePayload = array_merge($purchasePayload, [
                'discount' => $payload['discount'],
                'tax' => $payload['tax'],
                'other' => $payload['other'],
                'total_amount' => $payload['total_amount'],
                'pay_amount' => $payload['pay_amount'],
                'payment_method' => $payload['payment_method'],
                'bank_type' => $payload['payment_method'] === 'BANK' ? $payload['bank_type'] : null,
                'bank_account' => $payload['payment_method'] === 'BANK' ? $payload['bank_account'] : null,
                'bank_holdername' => $payload['payment_method'] === 'BANK' ? $payload['bank_holdername'] : null,
                'credit' => $payload['total_amount'] - $payload['pay_amount'],
                'deposit' => $payload['total_amount'] === $payload['pay_amount'] ? 0 : $payload['pay_amount'],
            ]);

            $purchase = Purchase::create($purchasePayload);

            $purchaseItemPayload = collect($payload['purchase_items'])->map(function ($item) use ($purchase) {
                return [
                    'purchase_id' => $purchase['id'],
                    'item_id' => $item['id'],
                    'price' => $item['price'],
                    'qty' => $item['qty'],
                    'total' => $item['price'] * $item['qty'],
                ];
            });

            $purchaseItem = PurchaseItem::insert($purchaseItemPayload->toArray());
            $purchase['purchase_items'] = $purchaseItemPayload->toArray();
            DB::commit();

            return $this->success('new purchase item is successfully created', $purchase);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show purchase item record by id
     */
    public function show($id)
    {
        DB::beginTransaction();

        try {
            $purchase = Purchase::FindOrFail($id);
            DB::commit();

            return $this->success('purchase item is successfully retrived', $purchase);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update purchase item record
     *
     * @bodyParam contact_person_id.
     * @bodyParam business_id.
     * @bodyParam item_id.
     * @bodyParam qty.
     * @bodyParam price.
     * @bodyParam status.
     */
    public function update(PurchaseUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $purchase = Purchase::FindOrFail($id);
            $purchase->update($payload->toArray());
            DB::commit();

            return $this->success('purchase item is successfully updated', $purchase);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete purchase item record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $purchase = Purchase::FindOrFail($id);
            $purchase->delete($id);
            DB::commit();

            return $this->success('purchase item is successfully deleted', $Purchase);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
