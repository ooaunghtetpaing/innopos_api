<?php

namespace App\Http\Controllers;

use App\Http\Requests\GeneralItemStoreRequest;
use App\Http\Requests\GeneralItemUpdateRequest;
use App\Models\GeneralItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Item management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class GeneralItemController extends Controller
{
    /**
     * APIs for retrive general item records
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,label,status,description
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $generalItem = GeneralItem::with(['category'])
                ->searchQuery()
                ->sortingQuery()
                ->paginationQuery();
            DB::commit();

            return $this->success('general items are successfully retrived', $generalItem);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

    }

    /**
     * APIs for create new general item record
     *
     * @bodyParam item_category_id required.
     * @bodyParam name required.
     * @bodyParam item_cover_photo.
     * @bodyParam item_photos.
     * @bodyParam model required.
     * @bodyParam code required.
     * @bodyParam label.
     * @bodyParam description.
     * @bodyParam other_name.
     * @bodyParam make_in.
     * @bodyParam packing.
     * @bodyParam pebox.
     */
    public function store(GeneralItemStoreRequest $request)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $generalItem = GeneralItem::create($payload->toArray());
            DB::commit();

            return $this->success('new general item is successfully created', $generalItem);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show general item record by id
     */
    public function show($id)
    {
        DB::beginTransaction();

        try {
            $generalItem = GeneralItem::with(['category'])
                ->FindOrFail($id);
            DB::commit();

            return $this->success('general item is successfully retrived', $generalItem);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create new general item record
     *
     * @bodyParam item_category_id required.
     * @bodyParam name required.
     * @bodyParam item_cover_photo.
     * @bodyParam item_photos.
     * @bodyParam model required.
     * @bodyParam code required.
     * @bodyParam label.
     * @bodyParam description.
     * @bodyParam other_name.
     * @bodyParam make_in.
     * @bodyParam packing.
     * @bodyParam pebox.
     * @bodyParam stats.
     */
    public function update(GeneralItemUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $generalItem = GeneralItem::FindOrFail($id);
            $generalItem->update($payload->toArray());
            DB::commit();

            return $this->success('general item is successfully updated', $generalItem);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete general item record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $generalItem = GeneralItem::FindOrFail($id);
            $generalItem->delete($id);
            DB::commit();

            return $this->success('general item is successfully deleted', $generalItem);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
