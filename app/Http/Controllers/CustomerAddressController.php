<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerAddressStoreRequest;
use App\Http\Requests\CustomerAddressUpdateRequest;
use App\Models\CustomerAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Customer management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class CustomerAddressController extends Controller
{
    /**
     * APIs for retrive customers address record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $customerAddress = CustomerAddress::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('Customer address are successfully retrived', $customerAddress);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create new customer address record
     *
     * @bodyParam customer_id required.
     * @bodyParam address required.
     */
    public function store(CustomerAddressStoreRequest $request)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $customerAddress = CustomerAddress::create($payload->toArray());
            DB::commit();

            return $this->success('New customer address is created successfully', $customerAddress);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show customer address record by id
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $customerAddress = CustomerAddress::findOrFail($id);
            DB::commit();

            return $this->success('Customer address info is retrieved successfully', $customerAddress);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update customer address record
     *
     * @bodyParam customer_id.
     * @bodyParam address.
     * @bodyParam status.
     */
    public function update(CustomerAddressUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $customer = CustomerAddress::findOrFail($id);

            if ($payload['is_default'] === true) {
                $defualtAddress = CustomerAddress::where(['is_default' => true])->first();

                if ($defualtAddress) {
                    $defualtAddress->update(['is_default' => false]);
                }
            }

            $customer->update($payload->toArray());
            DB::commit();

            return $this->success('Customer address is updated successfully', $customer);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete customer address record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $customer = CustomerAddress::findOrFail($id);
            $customer->delete($id);
            DB::commit();

            return $this->success('customer is deleted', $customer);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
