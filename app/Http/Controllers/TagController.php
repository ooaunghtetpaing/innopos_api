<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagStoreRequest;
use App\Http\Requests\TagUpdateRequest;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    /**
     * APIs for retrive item category record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,label,status,description
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     * @urlParam status string. Example: ALL, ACTIVE, DISABLE, DELETED, RESTORE
     */
    public function index(Request $request)
    {
        DB::beginTransaction();

        try {
            $tags = Tag::searchQuery()
                ->sortingQuery()
                ->paginationQuery();
            DB::commit();

            return $this->success('tags are successfully retrived', $tags);
        } catch (Exception $e) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * APIs for create new tag record
     *
     * @bodyParam tag required.
     */
    public function store(TagStoreRequest $request)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $tag = Tag::create($payload->toArray());
            DB::commit();

            return $this->success('new tag is successfully created', $tag);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * APIs for show tag record by id
     */
    public function show($id)
    {
        DB::beginTransaction();

        try {
            $tag = Tag::FindOrFail($id);
            DB::commit();

            return $this->success('tag detail is successfully retrived', $tag);
        } catch (Exception $e) {
            DB::rollBack();

            return $e;
        }
    }

    /**
     * APIs for update tag record by id
     *
     * @bodyParam tag.
     */
    public function update(TagUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $tag = Tag::FindOrFail($id);
            $tag->update($payload->toArray());
            DB::commit();

            return $this->success('tag is successfully updated', $tag);
        } catch (Exception $e) {
            DB::rollBack();

            return $e;
        }
    }

    /**
     * APIs for delete tag record by id
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $tag = Tag::FindOrFail($id);
            $tag->delete($id);
            DB::commit();

            return $this->success('tag is successfully deleted', $tag);
        } catch (Exception $e) {
            DB::rollBack();

            return $e;
        }
    }
}
