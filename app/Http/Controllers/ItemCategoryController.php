<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemCategoryStoreRequest;
use App\Http\Requests\ItemCategoryUpdateRequest;
use App\Models\ItemCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Item management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class ItemCategoryController extends Controller
{
    /**
     * APIs for retrive item category record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,label,status,description
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {
        DB::beginTransaction();

        try {
            $itemCategories = ItemCategory::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('item categories are successfully retrived', $itemCategories);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

    }

    /**
     * APIs for create new item category record
     *
     * @bodyParam name required.
     * @bodyParam tag.
     * @bodyParam remark.
     */
    public function store(ItemCategoryStoreRequest $request)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();

        try {
            $itemCategory = ItemCategory::create($payload->toArray());
            DB::commit();

            return $this->success('new item category is successfully created', $itemCategory);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show item category record by id
     */
    public function show($id)
    {
        DB::beginTransaction();

        try {
            $itemCategory = ItemCategory::with(['items'])->FindOrFail($id);
            DB::commit();

            return $this->success('item category is successfully retrived', $itemCategory);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update user record
     *
     * @bodyParam name.
     * @bodyParam tag.
     * @bodyParam remark.
     * @bodyParam status.
     */
    public function update(ItemCategoryUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $itemCategory = ItemCategory::FindOrFail($id);
            $itemCategory->update($payload->toArray());
            DB::commit();

            return $this->success('item category is successfully updated', $itemCategory);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete item category record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $itemCategory = ItemCategory::FindOrFail($id);
            $itemCategory->delete($id);
            DB::commit();

            return $this->success('item category is successfully deleted', $itemCategory);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
