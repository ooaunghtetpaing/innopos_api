<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileStoreRequest;
use App\Models\File;
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{
    /**
     * APIs for retrive file record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index()
    {
        DB::beginTransaction();
        try {
            $file = File::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('file list are successfully retrived', $file);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create new file record
     *
     * @bodyParam file.
     */
    public function store(FileStoreRequest $request)
    {
        $payload = collect($request->validated());
        $file = $payload['file'];

        $image_path = $file->store('images', 'public');
        $name = explode('/', $image_path)[1];

        DB::beginTransaction();
        try {
            $file = File::create([
                'name' => $name,
                'size' => $file->getSize(),
                'type' => $file->getMimeType(),
            ]);
            DB::commit();

            return $this->success('file is created successfully', $file);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function show($name)
    {
        DB::beginTransaction();
        try {
            $file = File::where(['name' => $name])->first();
            $image = file_get_contents(public_path('storage')."/images/$file->name");
            DB::commit();

            return response($image, 200)->header('Content-Type', $file->type);

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete file record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $file = File::findOrFail($id);
            $file->delete($id);
            DB::commit();

            return $this->success('File is deleted', $file);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
