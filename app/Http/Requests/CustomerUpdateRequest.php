<?php

namespace App\Http\Requests;

use App\Enums\GeneralStatusEnum;
use App\Enums\REGXEnum;
use App\Helpers\Enum;
use App\Models\Customer;
use Illuminate\Foundation\Http\FormRequest;

class CustomerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $mobileRule = REGXEnum::MOBILE_NUMBER->value;

        $generalStatusEnum = implode(',', (new Enum(GeneralStatusEnum::class))->values());

        $customer = Customer::findOrFail(request()->id);
        $customerId = $customer->id;

        return [
            'full_name' => 'string|min:3',
            'email' => "nullable|email|unique:customers,email,$customerId",
            'phone' => "regex:$mobileRule | unique:customers,phone,$customerId",
            'company_name' => 'nullable|string|min:3',
            'position' => 'nullable|string|min:3',
            'address' => 'nullable|string|min:3',
            'status' => "string | in:$generalStatusEnum",
        ];
    }
}
