<?php

namespace App\Http\Requests;

use App\Enums\GeneralStatusEnum;
use App\Helpers\Enum;
use App\Models\Customer;
use App\Models\CustomerAddress;
use Illuminate\Foundation\Http\FormRequest;

class CustomerAddressUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $customerIds = implode(',', Customer::all()->pluck('id')->toArray());

        $customerAddress = CustomerAddress::findOrFail(request('id'));
        $customerAddressId = $customerAddress->id;

        $generalStatusEnum = implode(',', (new Enum(GeneralStatusEnum::class))->values());

        return [
            'customer_id' => "in:$customerIds",
            'address' => 'string',
            'status' => "string | in:$generalStatusEnum",
            'is_default' => 'boolean | nullable',
        ];
    }
}
