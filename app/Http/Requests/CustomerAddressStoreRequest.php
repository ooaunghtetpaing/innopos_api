<?php

namespace App\Http\Requests;

use App\Models\Customer;
use Illuminate\Foundation\Http\FormRequest;

class CustomerAddressStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $customers = implode(',', Customer::all()->pluck('id')->toArray());

        return [
            'customer_id' => "required | in:$customers",
            'address' => 'required | string',
            'is_default' => 'boolean | nullable',
        ];
    }
}
