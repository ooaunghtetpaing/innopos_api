<?php

namespace App\Http\Requests;

use app\Models\Tag;
use Illuminate\Foundation\Http\FormRequest;

class PurchaseUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $tag = Tag::findOrFail(request()->id);
        $tagId = $tag->id;

        return [
            'tag' => "string | unique:tags,tag,$tagId",
        ];
    }
}
