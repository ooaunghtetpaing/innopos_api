<?php

namespace App\Http\Requests;

use App\Enums\BusinessTypeEnum;
use App\Enums\GeneralStatusEnum;
use App\Enums\REGXEnum;
use App\Helpers\Enum;
use App\Models\Business;
use Illuminate\Foundation\Http\FormRequest;

class BusinessUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $mobileRule = REGXEnum::MOBILE_NUMBER->value;

        $businessTypeEnums = implode(',', (new Enum(BusinessTypeEnum::class))->values());
        $generalStatusEnums = implode(',', (new Enum(GeneralStatusEnum::class))->values());

        $business = Business::findOrFail(request()->id);
        $businessId = $business->id;

        return [
            'name' => "string | unique:businesses,name,$businessId",
            'phone' => ["regex:$mobileRule", 'required', "unique:businesses,phone,$businessId"],
            'email' => "string | unique:businesses,email,$businessId",
            'address' => 'string',
            'type' => "string | in:$businessTypeEnums",
            'status' => "string | in:$generalStatusEnums",
        ];
    }
}
