<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralItemStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {

        return [
            'item_category_id' => 'required | exists:item_categories,id',
            'name' => 'required | string',
            'model' => 'required | string',
            'code' => 'required | unique:general_items,code',
            'label' => 'string | nullable',
            'description' => 'string  | nullable',
            'other_name' => 'string | nullable',
            'make_in' => 'string | nullable',
            'packing' => 'numeric | nullable',
            'prebox' => 'numeric | nullable',
        ];
    }
}
