<?php

namespace App\Http\Requests;

use App\Enums\BusinessTypeEnum;
use App\Enums\REGXEnum;
use App\Helpers\Enum;
use Illuminate\Foundation\Http\FormRequest;

class BusinessStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $mobileRule = REGXEnum::MOBILE_NUMBER->value;

        $businessTypeEnums = implode(',', (new Enum(BusinessTypeEnum::class))->values());

        return [
            'name' => 'required | string | unique:businesses,name',
            'phone' => ["regex:$mobileRule", 'required', 'unique:businesses,phone'],
            'email' => 'required | string | unique:businesses,email',
            'address' => 'string',
            'type' => "string | in:$businessTypeEnums",
        ];
    }
}
