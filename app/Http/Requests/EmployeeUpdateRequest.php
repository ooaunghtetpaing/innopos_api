<?php

namespace App\Http\Requests;

use App\Enums\EmployeeStatusEnum;
use App\Helpers\Enum;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $employeeStatusEnum = implode(',', (new Enum(EmployeeStatusEnum::class))->values());

        $employee = Employee::findOrFail(request()->id);
        $employeeId = $employee->id;

        $userIds = implode(',', User::all()->pluck('id')->toArray());

        return [
            'user_id' => "in:$userIds|unique:employees,user_id,$employeeId",
            'profile_picture' => 'string | nullable',
            'father_name' => 'nullable|string',
            'mother_name' => 'nullable|string',
            'dob' => 'nullable|date',
            'position' => 'nullable|string',
            'address' => 'nullable|string',
            'nrc' => 'nullable|string',
            'status' => "nullable|string|in:$employeeStatusEnum",
            'salaray' => 'nullable | numeric',
        ];
    }
}
