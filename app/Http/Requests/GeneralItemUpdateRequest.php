<?php

namespace App\Http\Requests;

use App\Enums\GeneralStatusEnum;
use App\Helpers\Enum;
use App\Models\GeneralItem;
use App\Models\ItemCategory;
use Illuminate\Foundation\Http\FormRequest;

class GeneralItemUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {

        $generalStatus = implode(',', (new Enum(GeneralStatusEnum::class))->values());

        $itemCategoryIds = implode(',', ItemCategory::all()->pluck('id')->toArray());

        $generalItem = GeneralItem::findOrFail(request()->id);
        $generalItemId = $generalItem->id;

        return [
            'item_category_id' => "in:$itemCategoryIds",
            'name' => 'string',
            'item_cover_photo' => 'string | nullable',
            'item_photos' => 'array | nullable',
            'model' => 'string',
            'code' => "unique:general_items,code,$generalItemId",
            'label' => 'nullable',
            'description' => 'string  | nullable',
            'other_name' => 'string | nullable',
            'make_in' => 'string | nullable',
            'packing' => 'numeric | nullable',
            'prebox' => 'numeric | nullable',
            'is_inventory' => 'boolean | nullable',
            'status' => "string | in:$generalStatus",
        ];
    }
}
