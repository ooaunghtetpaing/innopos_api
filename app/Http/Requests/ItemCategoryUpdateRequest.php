<?php

namespace App\Http\Requests;

use App\Enums\GeneralStatusEnum;
use App\Helpers\Enum;
use App\Models\ItemCategory;
use Illuminate\Foundation\Http\FormRequest;

class ItemCategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $itemCategory = ItemCategory::findOrFail(request()->id);
        $itemCategoryId = $itemCategory->id;

        $generalStatusEnum = implode(',', (new Enum(GeneralStatusEnum::class))->values());

        return [
            'name' => "required | unique:item_categories,name,$itemCategoryId",
            'remark' => 'string | nullable',
            'status' => "string | in:$generalStatusEnum",
        ];
    }
}
