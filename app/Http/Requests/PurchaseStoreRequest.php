<?php

namespace App\Http\Requests;

use App\Enums\BankTypeEnum;
use App\Enums\PaymentMethodEnum;
use App\Helpers\Enum;
use App\Models\Business;
use App\Models\ContactPerson;
use App\Models\GeneralItem;
use Illuminate\Foundation\Http\FormRequest;

class PurchaseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $contactPersonIds = implode(',', ContactPerson::all()->pluck('id')->toArray());
        $businessIds = implode(',', Business::all()->pluck('id')->toArray());
        $paymentMethods = implode(',', (new Enum(PaymentMethodEnum::class))->values());
        $banktype = implode(',', (new Enum(BankTypeEnum::class))->values());
        $items = GeneralItem::all()->pluck('id');

        return [
            'business_id' => "required | in:$businessIds",
            'contact_id' => "in:$contactPersonIds | nullable",
            'discount' => 'numeric | nullable',
            'tax' => 'numeric | nullable',
            'other' => 'numeric | nullable',
            'total_amount' => 'numeric | required',
            'pay_amount' => 'numeric',
            'payment_method' => "required | in:$paymentMethods",
            'bank_type' => "in:$banktype | required_if:payment_method,==,BANK",
            'bank_holdername' => 'string | required_if:payment_method,==,BANK',
            'bank_account' => 'string | required_if:payment_method,==,BANK',
            'purchase_items' => 'required | array',
            'purchase_items.*.id' => "required | in:$items",
            'purchase_items.*.price' => 'required | numeric',
            'purchase_items.*.qty' => 'required | numeric',
        ];
    }
}
