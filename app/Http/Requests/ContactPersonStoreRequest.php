<?php

namespace App\Http\Requests;

use App\Enums\REGXEnum;
use App\Models\Business;
use Illuminate\Foundation\Http\FormRequest;

class ContactPersonStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $mobileRule = REGXEnum::MOBILE_NUMBER->value;

        $businessIds = implode(',', Business::all()->pluck('id')->toArray());

        return [
            'business_id' => "required | in:$businessIds",
            'name' => 'required | string',
            'phone' => ['required', 'string', "regex:$mobileRule", 'unique:contact_people,phone'],
            'email' => 'string | unique:contact_people,email',
            'position' => 'string | nullable',
            'address' => 'string | nullable',
        ];
    }
}
