<?php

namespace App\Http\Requests;

use App\Enums\GeneralStatusEnum;
use App\Enums\REGXEnum;
use App\Helpers\Enum;
use App\Models\Business;
use App\Models\ContactPerson;
use Illuminate\Foundation\Http\FormRequest;

class ContactPersonUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $mobileRule = REGXEnum::MOBILE_NUMBER->value;

        $businessIds = implode(',', Business::all()->pluck('id')->toArray());

        $contactPerson = ContactPerson::findOrFail(request()->id);
        $contactPersonId = $contactPerson->id;

        $generalStatusEnum = implode(',', (new Enum(GeneralStatusEnum::class))->values());

        return [
            'business_id' => "in:$businessIds",
            'name' => 'string',
            'phone' => ["unique:contact_people,phone,$contactPersonId", 'string', "regex:$mobileRule"],
            'email' => "unique:contact_people,email,$contactPersonId",
            'position' => 'string',
            'address' => 'string',
            'status' => "string| in:$generalStatusEnum",
        ];
    }
}
