<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GeneralItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\GeneralItem::factory(env('NUM_FACTORY', 100))->create();
    }
}
