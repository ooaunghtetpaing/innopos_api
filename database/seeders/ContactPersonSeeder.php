<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactPersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\ContactPerson::factory(env('NUM_FACTORY', 100))->create();
    }
}
