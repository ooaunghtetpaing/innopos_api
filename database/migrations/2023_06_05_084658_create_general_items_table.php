<?php

use App\Enums\GeneralItemStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('general_items', function (Blueprint $table) {
            $table->snowflakeIdAndPrimary();
            $table->snowflakeId('item_category_id');
            $table->string('name');
            $table->string('item_cover_photo')->nullable()->default(null);
            $table->json('item_photos')->nullable()->default(null);
            $table->string('model');
            $table->string('code')->unique();
            $table->string('label')->nullable()->default(null);
            $table->longtext('description')->nullable()->default(null);
            $table->string('other_name')->nullable()->default(null);
            $table->string('make_in')->nullable()->default(null);
            $table->integer('packing')->nullable()->default(null);
            $table->integer('prebox')->nullable()->default(null);
            $table->boolean('is_inventory')->nullable()->default(false);
            $table->string('status')->default(GeneralItemStatusEnum::ACTIVE->value);
            $table->auditColumns();

            $table->foreign('item_category_id')->references('id')->on('item_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('general_items');
    }
};
