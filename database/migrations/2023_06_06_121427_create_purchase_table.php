<?php

use App\Enums\PurchaseStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('purchase', function (Blueprint $table) {
            $table->snowflakeIdAndPrimary();
            $table->string('contact_person')->nullable()->default()->null;
            $table->string('contact_person_phone')->nullable()->default()->null;
            $table->string('contact_person_email')->nullable()->default()->null;
            $table->string('business_name');
            $table->string('business_type');
            $table->string('business_phone')->nullable()->default()->null;
            $table->string('business_email')->nullable()->default()->null;
            $table->string('business_address')->nullable()->default()->null;
            $table->string('payment_method')->nullable()->default()->null;
            $table->string('bank_type')->nullable()->default()->null;
            $table->string('bank_account')->nullable()->default()->null;
            $table->string('bank_holdername')->nullable()->default()->null;
            $table->decimal('discount', 19, 4)->nullable()->default(0);
            $table->decimal('tax', 19, 4)->nullable()->default(0);
            $table->decimal('other', 19, 4)->nullable()->default(0);
            $table->decimal('total_amount', 19, 4);
            $table->decimal('pay_amount', 19, 4)->nullable()->default(0);
            $table->decimal('deposit', 19, 4)->nullable()->default(0);
            $table->decimal('credit', 19, 4)->nullable()->default(0);
            $table->string('status')->default(PurchaseStatusEnum::APPROVED->value);
            $table->auditColumns();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('purchase');
    }
};
