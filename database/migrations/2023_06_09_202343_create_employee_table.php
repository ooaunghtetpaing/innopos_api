<?php

use App\Enums\EmployeeStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->snowflakeIdAndPrimary();
            $table->snowflakeId('user_id')->unique();
            $table->string('profile_photo')->nullable()->default(null);
            $table->string('father_name')->nullable()->default(null);
            $table->string('mother_name')->nullable()->default(null);
            $table->date('dob')->nullable()->default(null);
            $table->string('nrc')->nullable()->default(null);
            $table->string('position')->nullable()->default(null);
            $table->float('salary')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('status')->default(EmployeeStatusEnum::PENDING->value);
            $table->auditColumns();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
    }
};
