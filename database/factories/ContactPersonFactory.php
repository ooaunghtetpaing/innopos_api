<?php

namespace Database\Factories;

use App\Models\Business;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ContactPersonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $contact = Business::all()->pluck('id');
        $position = ['HR', 'Manager', 'Developer', 'Desinger', 'ContactWriter'];

        return [
            'business_id' => $contact[rand(0, 99)],
            'name' => fake()->name(),
            'phone' => fake()->unique()->phoneNumber(),
            'email' => fake()->unique()->email(),
            'address' => fake()->address(),
            'position' => $position[rand(0, 4)],
        ];
    }
}
