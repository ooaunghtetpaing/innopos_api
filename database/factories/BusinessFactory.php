<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Business>
 */
class BusinessFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $type = ['COMPANY', 'WHOLESALE'];

        return [
            'name' => fake()->unique()->name(),
            'phone' => fake()->unique()->phoneNumber(),
            'email' => fake()->unique()->email(),
            'address' => fake()->address(),
            'type' => $type[rand(0, 1)],
        ];
    }
}
