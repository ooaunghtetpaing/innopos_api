<?php

namespace Database\Factories;

use App\Models\ItemCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class GeneralItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $general = ItemCategory::all()->pluck('id');

        return [
            'item_category_id' => $general[rand(0, 99)],
            'name' => $this->arrayToString(fake()->words(2)),
            'model' => $this->arrayToString(fake()->words(1)),
            'code' => fake()->ean13(),
        ];
    }

    protected function arrayToString($array)
    {
        return implode(' ', $array);
    }
}
