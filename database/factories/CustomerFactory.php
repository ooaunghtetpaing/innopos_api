<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $position = ['HR', 'Manager', 'Developer', 'Desinger', 'ContactWriter'];

        return [
            'full_name' => fake()->name(),
            'email' => fake()->unique()->email(),
            'phone' => fake()->unique()->phoneNumber(),
            'company_name' => fake()->name(),
            'position' => $position[rand(0, 4)],
        ];
    }
}
